import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_otp/flutter_otp.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
// Now instantiate FlutterOtp class in order to call sendOtp function
  FlutterOtp otp = FlutterOtp();
  String phoneNumber = ""; //enter your 10 digit number
  int minNumber = 1000;
  int maxNumber = 6000;
  var _textController = TextEditingController();
  var _textControllerotp = TextEditingController();
  String countryCode = "+91";

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    var rng = new Random();
    var code = rng.nextInt(900000) + 100000;

    return Scaffold(
      appBar: AppBar(title: Text(" otp  ")),
      body: Container(
          child: Column(
        children: [
          new TextField(
            controller: _textController,
            decoration: new InputDecoration(labelText: "Enter your number"),
            keyboardType: TextInputType.number,
            maxLength: 10,
          ),
          Center(
              child: RaisedButton(
            child: Text("Send"),
            onPressed: () {
              // call sentOtp function and pass the parameters

              otp.sendOtp(_textController.text, 'OTP is $code', minNumber,
                  maxNumber, countryCode);
            },
          )),
          new TextField(
            controller: _textControllerotp,
            decoration: new InputDecoration(labelText: "Enter your otp"),
            keyboardType: TextInputType.number,
            maxLength: 6,
          ),
          Center(
              child: RaisedButton(
            child: Text("Check"),
            onPressed: () {
              // call sentOtp function and pass the parameters

              int value = int.parse(_textControllerotp.text);
              if (value == code) {
                Fluttertoast.showToast(
                    msg: "Authenticate Successfully",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.CENTER,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 16.0);
              } else {
                Fluttertoast.showToast(
                    msg: "Authentication Failed",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.CENTER,
                    backgroundColor: Colors.red,
                    textColor: Colors.white,
                    fontSize: 16.0);
              }
            },
          )),
        ],
      )),
    );
  }
}
